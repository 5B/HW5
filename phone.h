#include <stdio.h>
#include <string.h>
#include<stdlib.h>
#define MAX_NUM 100
#define SIZE_NAME 100
#define SIZE_NUMBER 100
 
typedef struct{
	     char name[SIZE_NAME];
		      char number[SIZE_NUMBER];
			  	 struct Phone * next;
}Phone;

Phone* Register(Phone * ptr, int * num);
//void continue_Register(Phone * ptr, int * num);
Phone* Delete(Phone * ptr, int * num);

int Search(Phone * ptr, int* num);

int saveFile(Phone* ptr, int* num);
Phone * openFile(Phone* ptr, int* num);

